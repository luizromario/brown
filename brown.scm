; tagging functions and aliases

(define tagit cons)
(define tag car)

(define body cdr)
(define value-of car)
(define first car)
(define second cadr)
(define rest cdr)

(define call/cc call-with-current-continuation)
(define (wrap/quote v) (list 'quote v))

(define (I x) x)

; replacements for obsolete scheme procedures

(define (atom? x)
  (and (not (pair? x))
       (not (null? x))
       (not (procedure? x))))

(define (reset) ;TODO
  '())

(define (writeln . args)
  (letrec ((loop (lambda (args)
                   (if (not (null? args))
                       (begin (display (car args))
                              (loop (cdr args)))))))
    (loop args))
  (newline))

; syntactic auxiliaries

(define (abs? f)
  (eq? (tag f) 'abs))
(define (reifier? f)
  (eq? (tag f) 'reifier))
(define (simple? f)
  (eq? (tag f) 'simple))

(define (syntactic-type exp)
  (cond
    ((atom? exp) 'identifier)
    ((abs? exp) 'abstraction)
    (else 'application)))

; the interpreter

(define (<identifier> exp)
  (lambda (env cont)
    (env exp (lambda (cell)
               (cont (value-of cell))))))

(define (<abs> abs-builder exp)
  (lambda (env cont)
    (cont (abs-builder (lambda (v* c)
                         ((denotation (second exp))
                          (ext env (first exp) v*)
                          c))))))

(define (<app> exp)
  (lambda (env cont)
    ((denotation (first exp)) env (lambda (f)
                                    (f (rest exp) env cont)))))

(define (<reify> fun)
  (lambda (exp env cont)
    (fun (list exp (schemeU-to-brown env) (schemeK-to-brown cont))
         wrong)))

(define (<simple> fun)
  (lambda (exp env cont)
    (letrec ((loop (lambda (exp cont)
                     (if (null? exp) (cont '())
                         ((denotation (first exp))
                          env
                          (lambda (v)
                            (loop (rest env)
                                  (lambda (w)
                                    (cont (cons v w))))))))))
      (loop exp (lambda (v*) (fun v* cont))))))

(define (denotation exp)
  (case (syntactic-type exp)
    (identifier (<identifier> exp))
    (abstraction
     (let ((b (body exp)))
       (<abs> (if (reifier? b)
                  <reify>
                  <simple>)
              (body b))))
    (application (<app> exp))))

(define schemeF-to-brown <simple>)

(define (brown-to-schemeF bf)
  (lambda (v* c) (bf (map wrap/quote v*) initenv c)))

(define (schemeCont-to-brown cont)
  (lambda (exp env cont1)
    (if (= (length exp) 1)
        ((denotation (first exp)) env cont)
        (wrong (list "schemeCont-to-brown: wrong number of args " exp)))))

(define (brown-to-schemeCont bf)
  (lambda (v) (bf (list (wrap/quote v)) initenv I)))

(define (schemeEnv-to-brown env)
  (lambda (exp env1 cont1)
    (if (= (length exp) 1)
        ((denotation (first exp)) env1 (lambda (v)
                                         (env v cont1)))
        (wrong (list "schemeEnv-to-brown: wrong number of args " exp)))))

(define (brown-to-schemeEnv bf)
  (lambda (v c) (bf (wrap/quote v) initenv c)))

(define nullenv
  (lambda (v c) (wrong (list "brown: unbound id " v))))

(define (ext env vars vals)
  (if (= (length vars) (length vals))
      (lambda (v c)
        (letrec ((lookup (lambda (vars vals)
                           (cond ((null? vars) (env v c))
                                 ((eq? (first vars) v) (c vals))
                                 (else (lookup (rest vars) (rest vals)))))))
          (lookup vars vals)))
      (begin
        (writeln "Brown: wrong number of actuals")
        (writeln "Formals: " vars)
        (writeln "Actuals: " vals)
        (wrong "ext failed"))))

; auxiliaries for setting up initenv

; covnert direct scheme fcns to schemeF

(define (scheme-to-schemeF f)
  (lambda (v* c) (c (apply f v*))))

(define (define-brown1 name exp)
  (call/cc (lambda (caller)
             ((denotation exp)
              initenv
              (lambda (v)
                (set! initenv (ext initenv (list name) (list v)))
                (caller name))))))

(define-syntax define-brown
  (syntax-rules () ((_ id val)
                    (define-brown1 'id 'val))))

(define (run exp)
  (call/cc (lambda (caller)
             ((denotation exp) initenv caller))))

(define (wrong v)
  (writeln "wrong: " v) (reset))

; initenv

(define scheme-fn-table (list (cons 'car car)
                              (cons 'cdr cdr)
                              (cons 'cons cons)
                              (cons 'eq? eq?)
                              (cons 'atom? atom?)
                              (cons 'null? null?)
                              ;(cons 'add1 add1)
                              ;(cons 'sub1 sub1)
                              ;(cons '=0 =0)
                              (cons '+ +)
                              (cons '- -)
                              (cons '* *)
                              ;(cons 'print print)
                              (cons 'length length)
                              (cons 'read read)
                              (cons 'ext (lambda (br p* v*)
                                           (schemeEnv-to-brown
                                            (ext (brown-to-schemeEnv br)
                                                 p* v*))))
                              (cons 'nullenv nullenv)
                              (cons 'update-store (lambda (x y)
                                                    (value-of (set-car! x y))))
                              (cons 'reifier? reifier?)
                              (cons 'simple? simple?)
                              (cons 'abs? abs?)
                              (cons 'wrong wrong)
                              (cons 'ef (lambda (bool x y) (if bool x y)))
                              (cons 'newline newline)
                              (cons 'meaning (lambda (exp env cont)
                                               ((denotation exp) (brown-to-schemeEnv env)
                                                (brown-to-schemeCont cont))))))

(define initvars (map car scheme-fn-table))
(define initvals (map (lambda (x)
                        (schemeF-to-brown (scheme-to-schemeF (cdr x))))
                      scheme-fn-table))

(define initenv
  (ext (ext nullenv '(() t) '(() t)) initvars initvals))

(define-brown quote
  (abs reify (exp env cont) (cont (car exp))))
